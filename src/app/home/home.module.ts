import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { LoadersCssModule } from 'angular2-loaders-css';

import { HomeRoutingModule } from './home.routing';
import { NavComponent } from './nav.component';
import { AccesosService } from './admin/_services/accesos.service';
import { UsersService } from './admin/_services/users.service';

import { VendedorGuard } from "./../_guards/vendedor.guard";
import { AdminGuard } from "./../_guards/admin.guard";
import { ClienteGuard } from "./../_guards/cliente.guard";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoadersCssModule,
    HomeRoutingModule
  ],
  declarations: [NavComponent],
  providers: [
    VendedorGuard,
    AdminGuard,
    ClienteGuard,
    AccesosService,
    UsersService
  ]
})
export class HomeModule { }
