import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { VendedorComponent } from "./vendedor.component";
import { LlamadasComponent } from "./llamadas/llamadas.component";
import { CitasComponent } from "./citas/citas.component";

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: VendedorComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'llamadas', component: LlamadasComponent },
    { path: 'citas', component: CitasComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendedorRoutingModule { }
