import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from "angular2-datatable";
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoadersCssModule } from 'angular2-loaders-css';
import { ChartsModule } from 'ng2-charts';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

import { VendedorRoutingModule } from './vendedor.routing';

import { CitasService } from './_services/citas.service';
import { LlamadasService } from './_services/llamadas.service';

import { VendedorComponent } from './vendedor.component';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { CitasComponent } from './citas/citas.component';
import { LlamadasComponent } from './llamadas/llamadas.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    LoadersCssModule,
    AngularMultiSelectModule,
    VendedorRoutingModule
  ],
  declarations: [
    VendedorComponent,
    DashboardComponent,
    CitasComponent,
    LlamadasComponent,
    LoaderComponent
  ],
  providers: [
    CitasService,
    LlamadasService
  ]
})
export class VendedorModule { }
