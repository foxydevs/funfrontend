import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoLlamadaComponent } from './estado-llamada.component';

describe('EstadoLlamadaComponent', () => {
  let component: EstadoLlamadaComponent;
  let fixture: ComponentFixture<EstadoLlamadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoLlamadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoLlamadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
