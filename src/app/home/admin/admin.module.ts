import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from "angular2-datatable";
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoadersCssModule } from 'angular2-loaders-css';
import { ChartsModule } from 'ng2-charts';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

import { AdminRoutingModule } from './admin.routing';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { VentasComponent } from './ventas/ventas.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { LoaderComponent } from './loader/loader.component';
import { RolesComponent } from './roles/roles.component';
import { PuestosComponent } from './puestos/puestos.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ModulosComponent } from './modulos/modulos.component';
import { ProductosComponent } from './productos/productos.component';
import { EventosComponent } from './eventos/eventos.component';
import { EstadoLlamadaComponent } from './estado-llamada/estado-llamada.component';
import { EstadoCitaComponent } from './estado-cita/estado-cita.component';

import { UsersService } from "./_services/users.service";
import { EmployeesService } from "./_services/employees.service";
import { RolesService } from "./_services/roles.service";
import { PuestosService } from "./_services/puestos.service";
import { SucursalesService } from "./_services/sucursales.service";
import { ClientesService } from "./_services/clientes.service";
import { ProveedoresService } from "./_services/proveedores.service";
import { ModulosService } from "./_services/modulos.service";
import { AccesosService } from "./_services/accesos.service";
import { VentasService } from "./_services/ventas.service";
import { ProductosService } from "./_services/productos.service";
import { TiposProductoService } from "./_services/tipos-producto.service";
import { TiposVentaService } from "./_services/tipos-venta.service";
import { TiposCompraService } from "./_services/tipos-compra.service";
import { EventosService } from "./_services/eventos.service";
import { EstadoLlamadaService } from "./_services/estado-llamada.service";
import { EstadoCitaService } from "./_services/estado-cita.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    LoadersCssModule,
    AngularMultiSelectModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    DashboardComponent,
    UsuariosComponent,
    VentasComponent,
    ClientesComponent,
    ProveedoresComponent,
    EmpleadosComponent,
    LoaderComponent,
    RolesComponent,
    PuestosComponent,
    SucursalesComponent,
    PerfilComponent,
    ModulosComponent,
    ProductosComponent,
    EventosComponent,
    EstadoLlamadaComponent,
    EstadoCitaComponent
  ],
  providers: [
    UsersService,
    EmployeesService,
    RolesService,
    SucursalesService,
    PuestosService,
    ClientesService,
    ProveedoresService,
    ModulosService,
    AccesosService,
    VentasService,
    ProductosService,
    TiposProductoService,
    TiposVentaService,
    TiposCompraService,
    EventosService,
    EstadoLlamadaService,
    EstadoCitaService
  ]
})
export class AdminModule { }
