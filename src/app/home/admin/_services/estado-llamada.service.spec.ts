import { TestBed, inject } from '@angular/core/testing';

import { EstadoLlamadaService } from './estado-llamada.service';

describe('EstadoLlamadaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EstadoLlamadaService]
    });
  });

  it('should be created', inject([EstadoLlamadaService], (service: EstadoLlamadaService) => {
    expect(service).toBeTruthy();
  }));
});
