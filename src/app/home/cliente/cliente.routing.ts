import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ClienteComponent } from "./cliente.component";

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: ClienteComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    // { path: 'usuarios', component: UsuariosComponent },
    // { path: 'empleados', component: EmpleadosComponent },
    // { path: 'roles', component: RolesComponent },
    // { path: 'sucursales', component: SucursalesComponent },
    // { path: 'puestos', component: PuestosComponent },
    // { path: 'clientes', component: ClientesComponent },
    // { path: 'proveedores', component: ProveedoresComponent },
    // { path: 'perfil', component: PerfilComponent },
    // { path: 'ventas', component: VentasComponent },
    // { path: 'modulos', component: ModulosComponent },
    // { path: 'productos', component: ProductosComponent },
    // { path: 'eventos', component: EventosComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
